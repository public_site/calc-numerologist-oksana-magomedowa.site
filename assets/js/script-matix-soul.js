$(document).ready(function () {
    $("#FormCalc input[name=date]").inputmask("99.99.9999");
    $("#FormCalc2 input[name=date1]").inputmask("99.99.9999");
    $("#FormCalc2 input[name=date2]").inputmask("99.99.9999");

    $("#FormCalc input[name=date]").keyup(function (e) {
        var id = $(this).attr("id");
        var value = $(this).val();
        if (value.length > 0) {
            $("label[for=" + id + "]").hide();
        } else {
            $("label[for=" + id + "]").show();
        }
    });
    $("#FormCalc2 input[name=date1]").keyup(function (e) {
        var id = $(this).attr("id");
        var value = $(this).val();
        if (value.length > 0) {
            $("label[for=" + id + "]").hide();
        } else {
            $("label[for=" + id + "]").show();
        }
    });
    $("#FormCalc2 input[name=date2]").keyup(function (e) {
        var id = $(this).attr("id");
        var value = $(this).val();
        if (value.length > 0) {
            $("label[for=" + id + "]").hide();
        } else {
            $("label[for=" + id + "]").show();
        }
    });


    /*
    $("table.table-card-health").find('td').each(function () {
        let id = $(this).attr("id");
        if (id !== undefined) {
            if ($(this).html().length == 0) {
                $(this).html("#" + id);
            }
        }
    });
    $("table.table-card-period-energy").find('td').each(function () {
        let id = $(this).attr("id");
        if (id !== undefined) {
            if ($(this).html().length == 0) {
                $(this).html("#" + id);
            }
        }
    });
    */

    /**
     * Отправка формы даты рождения #FormCalc
     * 
     */
    $("#FormCalc").submit(function (e) {
        e.preventDefault();
        console.log("Отправка формы даты рождения #FormCalc");


        var value = $("#FormCalc input[name=date]").val();
        if (value.length == 0) {
            $("#FormCalc input[name=date]").addClass("is-invalid");
            return;
        } else {
            $("#FormCalc input[name=date]").removeClass("is-invalid");
        }


        var arr;
        var dateOfBirth = $(this).find("input[name=date]").val();
        var pointsMatrix = calcMatrixForString(dateOfBirth);

        /*
        var strDate = dateOfBirth + "";
        var aDate = strDate.split('.');
        if (aDate.length != 3) {
            alert("Не верный формат даты! Пример: 01.01.2020");
        }
        var nDay = parseInt(aDate[0]);
        var nMonth = parseInt(aDate[1]);
        var nYear = parseInt(aDate[2]);
        var day = nDay + "";
        if (nDay < 10) {
            day = "0" + day;
        }
        var month = nMonth + "";
        */

        // Первая точка (фиолетовый круг, в примере цифра 21)
        // #point_01
        // Это дата рождения с 1 до 22, если больше 22 – то есть: 23 – это 2+3 = 5 – вписывается цифра 5 и т.д.
        var point_01 = pointsMatrix.get("point_01");
        $("#point_01").html(point_01);
        $("#sahasraraPhysics").html(point_01);


        // Вторая точка (фиолетовый круг, в примере цифра 10)
        // #point_02
        // Это месяц рождения с 1 до 12
        var point_02 = pointsMatrix.get("point_02");
        $("#point_02").html(point_02);
        $("#sahasraraEnergy").html(point_02);

        // Третья точка (красный круг, в примере цифра 7)
        // #point_03
        // Это год рождения, вычисляется путем сложения: 
        // В нашем примере 1987 – то есть 1+ 9 + 8 + 7 = 25 и полученную цифру складываем пока не получится цифра в диапазоне от 1 до 22 включительно! 
        point_03 = pointsMatrix.get("point_03");
        $("#point_03").html(point_03);
        $("#muladharaPhysics").html(point_03);

        // Четвертая точка (красный круг, в примере цифра 11)
        // #point_04
        // Это сложение первых трех точек
        // В нашем примере это: 21+10+7 = 38, получилась цифра большее 22 - поэтому далее сводим путем сложения пока не получится цифра в диапазоне от 1 до 22: 3+8 = 11
        point_04 = pointsMatrix.get("point_04");
        $("#point_04").html(point_04);
        $("#muladharaEnergy").html(point_04);

        // Пятая точка (желтый круг по центру, в примере цифра 13)
        // #point_05
        // Это сложение первых четырех точек
        // В нашем примере это: 21+10+7+11 = 49, получилась цифра большее 22 - поэтому далее сводим путем сложения пока не получится цифра в диапазоне от 1 до 22: 4+9 = 13
        var point_05 = pointsMatrix.get("point_05");
        $("#point_05").html(point_05);
        $("#manipuraPhysics").html(point_05);
        $("#manipuraEnergy").html(point_05);

        // Шестая точка (голубой круг, в примере цифра 5)
        // #point_06
        // Это сложение 2ой и 5ой точки
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_06 = pointsMatrix.get("point_06");
        $("#point_06").html(point_06);
        $("#vishuddhaEnergy").html(point_06);

        // Седьмая точка (голубой круг, в примере цифра 7)
        // #point_07
        // Это сложение 1ой и 5ой точки
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_07 = pointsMatrix.get("point_07");
        $("#point_07").html(point_07);
        $("#vishuddhaPhysics").html(point_07);

        // Восьмая точка (оранжевый круг, в примере цифра 20)
        // #point_08
        // Складываем пятую (#point_05) точку и третью точку (#point_03)
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_08 = pointsMatrix.get("point_08");
        $("#point_08").html(point_08);
        $("#svadhisthanaPhysics").html(point_08);

        // Девятая точка – (оранжевый круг, в примере цифра 6)
        // #point_09
        // Складываем пятую (#point_05) точку и четвертую точку (#point_04)
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_09 = pointsMatrix.get("point_09");
        $("#point_09").html(point_09);
        $("#svadhisthanaEnergy").html(point_09);

        // Десятая точка (бесцветный круг, в примере цифра 8)
        // #point_10
        // Складываем точку #point_08 и точку #point_09
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_10 = pointsMatrix.get("point_10");
        $("#point_10").html(point_10);

        // Одиннадцатая точка (бесцветный круг, в примере цифра 10)
        // #point_11
        // Складываем точку #point_08 и точку #point_10
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_11 = pointsMatrix.get("point_11");
        $("#point_11").html(point_11);

        // Двенадцатая точка – (оранжевый круг, в примере цифра 14)
        // #point_12
        // Складываем точку #point_09 и точку #point_10
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_12 = pointsMatrix.get("point_12");
        $("#point_12").html(point_12);

        // Тринадцатая точка (бесцветный круг, в примере цифра 17) 
        // #point_13
        // Складываем четвертую точку и девятую точку
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_13 = pointsMatrix.get("point_13");
        $("#point_13").html(point_13);

        // Четырнадцатая точка (синий круг, в примере цифра 15)
        // #point_14
        // Складываем вторую точку и шестую
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_14 = pointsMatrix.get("point_14");
        $("#point_14").html(point_14);
        $("#ajnaEnergy").html(point_14);

        // Пятнадцатая точка – (синий круг, в примере цифра 10)
        // #point_15
        // Складываем первую и седьмую точку
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_15 = pointsMatrix.get("point_15");
        $("#point_15").html(point_15);
        $("#ajnaPhysics").html(point_15);

        // Шестнадцатая точка (зеленый круг, в примере цифра 18)
        // #point_16
        // Складываем пятую центральную точку и шестую
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_16 = pointsMatrix.get("point_16");
        $("#point_16").html(point_16);
        $("#anahataEnergy").html(point_16);

        // Семнадцатая точка (зеленый круг, в примере цифра 20)
        // #point_17
        // Складываем пятую центральную точку и седьмую
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_17 = pointsMatrix.get("point_17");
        $("#point_17").html(point_17);
        $("#anahataPhysics").html(point_17);

        // Восемнадцатая точка – (бесцветный круг, в примере цифра 4)
        // #point_18
        // Складываем первую и вторую точку
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_18 = pointsMatrix.get("point_18");
        $("#point_18").html(point_18);

        // Девятнадцатая точка (бесцветный круг, в примере цифра 17)
        // #point_19
        // Складываем вторую и третью точку
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_19 = pointsMatrix.get("point_19");
        $("#point_19").html(point_19);

        // Двадцатая точка (бесцветный круг, в примере цифра 18)
        // #point_20
        // Складываем третью и четвертую точку
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_20 = pointsMatrix.get("point_20");
        $("#point_20").html(point_20);

        // Двадцать первая точка – (бесцветный круг, в примере цифра 5)
        // #point_21
        // Складываем первую и четвертую точку
        // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
        var point_21 = pointsMatrix.get("point_21");
        $("#point_21").html(point_21);


        // Предназначение для себя - Небо
        // #SpanDestinyMySky
        // Складываем вторую и четвертую точку
        var destinyMySky = parseInt(point_02) + parseInt(point_04);
        destinyMySky = fixedNumberForRange(destinyMySky);
        $("#SpanDestinyMySky").html(destinyMySky);

        // Предназначение для себя - Земля
        // #SpanDestinyMyEarth
        // Складываем первую и третью точку
        var destinyMyEarth = parseInt(point_01) + parseInt(point_03);
        destinyMyEarth = fixedNumberForRange(destinyMyEarth);
        $("#SpanDestinyMyEarth").html(destinyMyEarth);

        // Предназначение для себя
        // #SpanDestinyMySum
        // Складываем предназначение для себя по небу и предназначение для себя по земле
        var destinyMySum = parseInt(destinyMySky) + parseInt(destinyMyEarth);
        destinyMySum = fixedNumberForRange(destinyMySum, 22);
        $("#SpanDestinyMySum").html(destinyMySum);


        // Предназначение для социума - М
        // #SpanDestinySocietyM
        // Складываем 20 и 18 точки
        var destinySocietyM = parseInt(point_20) + parseInt(point_18);
        destinySocietyM = fixedNumberForRange(destinySocietyM);
        $("#SpanDestinySocietyM").html(destinySocietyM);

        // Предназначение для социума - Ж
        // #SpanDestinySocietyZh
        // Складываем 21 и 19 точки
        var destinySocietyZh = parseInt(point_21) + parseInt(point_19);
        destinySocietyZh = fixedNumberForRange(destinySocietyZh);
        $("#SpanDestinySocietyZh").html(destinySocietyZh);




        // Предназначение для социума
        // #SpanDestinySocietySum
        // складываем предназначение для социума М и предназначение для социума Ж
        var destinySocietySum = parseInt(destinySocietyM) + parseInt(destinySocietyZh);
        destinySocietySum = fixedNumberForRange(destinySocietySum, 22);
        $("#SpanDestinySocietySum").html(destinySocietySum);

        // Общее предназначение (в примере цифра 12)
        // Складываем предназначение для себя третье и предназначение для социума третье

        var destinyGeneral = parseInt(destinyMySum) + parseInt(destinySocietySum);
        destinyGeneral = fixedNumberForRange(destinyGeneral, 22);
        $("#SpanDestinyGeneral").html(destinyGeneral);



        // Заполнение таблицы "Карта здоровья"
        let sahasraraHealthKey = calcValue(point_01, point_02);
        $("#sahasraraHealthKey").html(sahasraraHealthKey);
        let ajnaHealthKey = calcValue(point_15, point_14);
        $("#ajnaHealthKey").html(ajnaHealthKey);
        let vishuddhaHealthKey = calcValue(point_07, point_06);
        $("#vishuddhaHealthKey").html(vishuddhaHealthKey);
        let anahataHealthKey = calcValue(point_17, point_16);
        $("#anahataHealthKey").html(anahataHealthKey);
        let manipuraHealthKey = calcValue(point_05, point_05);
        $("#manipuraHealthKey").html(manipuraHealthKey);
        let svadhisthanaHealthKey = calcValue(point_08, point_09);
        $("#svadhisthanaHealthKey").html(svadhisthanaHealthKey);
        let muladharaHealthKey = calcValue(point_03, point_04);
        $("#muladharaHealthKey").html(muladharaHealthKey);
        $("#totalPhysics").html(calcTotalCardHealth(point_01, point_15, point_07, point_17, point_05, point_08, point_03));
        $("#totalEnergy").html(calcTotalCardHealth(point_02, point_14, point_06, point_16, point_05, point_09, point_04));
        $("#totalHealthKey").html(calcTotalCardHealth(sahasraraHealthKey, ajnaHealthKey, vishuddhaHealthKey, anahataHealthKey, manipuraHealthKey, svadhisthanaHealthKey, muladharaHealthKey));

        // Заполнение таблицы "Энергия периода"

        let tablePeriodEnergy_0 = point_01;
        let tablePeriodEnergy_10 = point_18;
        let tablePeriodEnergy_20 = point_02;
        let tablePeriodEnergy_30 = point_19;
        let tablePeriodEnergy_40 = point_03;
        let tablePeriodEnergy_50 = point_20;
        let tablePeriodEnergy_60 = point_04;
        let tablePeriodEnergy_70 = point_21;
        let tablePeriodEnergy_80 = point_01;

        let tablePeriodEnergy_5 = calcValue(tablePeriodEnergy_0, tablePeriodEnergy_10);
        let tablePeriodEnergy_15 = calcValue(tablePeriodEnergy_10, tablePeriodEnergy_20);
        let tablePeriodEnergy_25 = calcValue(tablePeriodEnergy_20, tablePeriodEnergy_30);
        let tablePeriodEnergy_35 = calcValue(tablePeriodEnergy_30, tablePeriodEnergy_40);
        let tablePeriodEnergy_45 = calcValue(tablePeriodEnergy_40, tablePeriodEnergy_50);
        let tablePeriodEnergy_55 = calcValue(tablePeriodEnergy_50, tablePeriodEnergy_60);
        let tablePeriodEnergy_65 = calcValue(tablePeriodEnergy_60, tablePeriodEnergy_70);
        let tablePeriodEnergy_75 = calcValue(tablePeriodEnergy_70, tablePeriodEnergy_80);

        let tablePeriodEnergy_2_5 = calcValue(tablePeriodEnergy_0, tablePeriodEnergy_5);
        let tablePeriodEnergy_7_5 = calcValue(tablePeriodEnergy_5, tablePeriodEnergy_10);
        let tablePeriodEnergy_12_5 = calcValue(tablePeriodEnergy_10, tablePeriodEnergy_15);
        let tablePeriodEnergy_17_5 = calcValue(tablePeriodEnergy_15, tablePeriodEnergy_20);
        let tablePeriodEnergy_22_5 = calcValue(tablePeriodEnergy_20, tablePeriodEnergy_25);
        let tablePeriodEnergy_27_5 = calcValue(tablePeriodEnergy_25, tablePeriodEnergy_30);
        let tablePeriodEnergy_32_5 = calcValue(tablePeriodEnergy_30, tablePeriodEnergy_35);
        let tablePeriodEnergy_37_5 = calcValue(tablePeriodEnergy_35, tablePeriodEnergy_40);
        let tablePeriodEnergy_42_5 = calcValue(tablePeriodEnergy_40, tablePeriodEnergy_45);
        let tablePeriodEnergy_47_5 = calcValue(tablePeriodEnergy_45, tablePeriodEnergy_50);
        let tablePeriodEnergy_52_5 = calcValue(tablePeriodEnergy_50, tablePeriodEnergy_55);
        let tablePeriodEnergy_57_5 = calcValue(tablePeriodEnergy_55, tablePeriodEnergy_60);
        let tablePeriodEnergy_62_5 = calcValue(tablePeriodEnergy_60, tablePeriodEnergy_65);
        let tablePeriodEnergy_67_5 = calcValue(tablePeriodEnergy_65, tablePeriodEnergy_70);
        let tablePeriodEnergy_72_5 = calcValue(tablePeriodEnergy_70, tablePeriodEnergy_75);
        let tablePeriodEnergy_77_5 = calcValue(tablePeriodEnergy_75, tablePeriodEnergy_0);


        let tablePeriodEnergy_1_25 = calcValue(tablePeriodEnergy_0, tablePeriodEnergy_2_5);
        let tablePeriodEnergy_3_75 = calcValue(tablePeriodEnergy_2_5, tablePeriodEnergy_5);
        let tablePeriodEnergy_6_25 = calcValue(tablePeriodEnergy_5, tablePeriodEnergy_7_5);
        let tablePeriodEnergy_8_75 = calcValue(tablePeriodEnergy_7_5, tablePeriodEnergy_10);
        let tablePeriodEnergy_11_25 = calcValue(tablePeriodEnergy_10, tablePeriodEnergy_12_5);
        let tablePeriodEnergy_13_75 = calcValue(tablePeriodEnergy_12_5, tablePeriodEnergy_15);
        let tablePeriodEnergy_16_25 = calcValue(tablePeriodEnergy_15, tablePeriodEnergy_17_5);
        let tablePeriodEnergy_18_75 = calcValue(tablePeriodEnergy_17_5, tablePeriodEnergy_20);
        let tablePeriodEnergy_21_25 = calcValue(tablePeriodEnergy_20, tablePeriodEnergy_22_5);
        let tablePeriodEnergy_23_75 = calcValue(tablePeriodEnergy_22_5, tablePeriodEnergy_25);
        let tablePeriodEnergy_26_25 = calcValue(tablePeriodEnergy_25, tablePeriodEnergy_27_5);
        let tablePeriodEnergy_28_75 = calcValue(tablePeriodEnergy_27_5, tablePeriodEnergy_30);
        let tablePeriodEnergy_31_25 = calcValue(tablePeriodEnergy_30, tablePeriodEnergy_32_5);
        let tablePeriodEnergy_33_75 = calcValue(tablePeriodEnergy_32_5, tablePeriodEnergy_35);
        let tablePeriodEnergy_36_25 = calcValue(tablePeriodEnergy_35, tablePeriodEnergy_37_5);
        let tablePeriodEnergy_38_75 = calcValue(tablePeriodEnergy_37_5, tablePeriodEnergy_40);
        let tablePeriodEnergy_41_25 = calcValue(tablePeriodEnergy_40, tablePeriodEnergy_42_5);
        let tablePeriodEnergy_43_75 = calcValue(tablePeriodEnergy_42_5, tablePeriodEnergy_45);
        let tablePeriodEnergy_46_25 = calcValue(tablePeriodEnergy_45, tablePeriodEnergy_47_5);
        let tablePeriodEnergy_48_75 = calcValue(tablePeriodEnergy_47_5, tablePeriodEnergy_50);
        let tablePeriodEnergy_51_25 = calcValue(tablePeriodEnergy_50, tablePeriodEnergy_52_5);
        let tablePeriodEnergy_53_75 = calcValue(tablePeriodEnergy_52_5, tablePeriodEnergy_55);
        let tablePeriodEnergy_56_25 = calcValue(tablePeriodEnergy_55, tablePeriodEnergy_57_5);
        let tablePeriodEnergy_58_75 = calcValue(tablePeriodEnergy_57_5, tablePeriodEnergy_60);
        let tablePeriodEnergy_61_25 = calcValue(tablePeriodEnergy_60, tablePeriodEnergy_62_5);
        let tablePeriodEnergy_63_75 = calcValue(tablePeriodEnergy_62_5, tablePeriodEnergy_65);
        let tablePeriodEnergy_66_25 = calcValue(tablePeriodEnergy_65, tablePeriodEnergy_67_5);
        let tablePeriodEnergy_68_75 = calcValue(tablePeriodEnergy_67_5, tablePeriodEnergy_70);
        let tablePeriodEnergy_71_25 = calcValue(tablePeriodEnergy_70, tablePeriodEnergy_72_5);
        let tablePeriodEnergy_73_75 = calcValue(tablePeriodEnergy_72_5, tablePeriodEnergy_75);
        let tablePeriodEnergy_76_25 = calcValue(tablePeriodEnergy_75, tablePeriodEnergy_77_5);
        let tablePeriodEnergy_78_75 = calcValue(tablePeriodEnergy_77_5, tablePeriodEnergy_80);







        $("#tablePeriodEnergy_0").html(tablePeriodEnergy_0);
        $("#tablePeriodEnergy_5").html(tablePeriodEnergy_5);
        $("#tablePeriodEnergy_10").html(tablePeriodEnergy_10);
        $("#tablePeriodEnergy_15").html(tablePeriodEnergy_15);
        $("#tablePeriodEnergy_20").html(tablePeriodEnergy_20);
        $("#tablePeriodEnergy_25").html(tablePeriodEnergy_25);
        $("#tablePeriodEnergy_30").html(tablePeriodEnergy_30);
        $("#tablePeriodEnergy_31_25").html(tablePeriodEnergy_31_25);
        $("#tablePeriodEnergy_32_5").html(tablePeriodEnergy_32_5);
        $("#tablePeriodEnergy_33_75").html(tablePeriodEnergy_33_75);
        $("#tablePeriodEnergy_35").html(tablePeriodEnergy_35);
        $("#tablePeriodEnergy_37_5").html(tablePeriodEnergy_37_5);
        $("#tablePeriodEnergy_36_25").html(tablePeriodEnergy_36_25);
        $("#tablePeriodEnergy_38_75").html(tablePeriodEnergy_38_75);
        $("#tablePeriodEnergy_40").html(tablePeriodEnergy_40);
        $("#tablePeriodEnergy_45").html(tablePeriodEnergy_45);
        $("#tablePeriodEnergy_50").html(tablePeriodEnergy_50);
        $("#tablePeriodEnergy_55").html(tablePeriodEnergy_55);
        $("#tablePeriodEnergy_60").html(tablePeriodEnergy_60);
        $("#tablePeriodEnergy_65").html(tablePeriodEnergy_65);
        $("#tablePeriodEnergy_70").html(tablePeriodEnergy_70);
        $("#tablePeriodEnergy_75").html(tablePeriodEnergy_75);
        $("#tablePeriodEnergy_80").html(tablePeriodEnergy_80);



        $("#tablePeriodEnergy_2_5").html(tablePeriodEnergy_2_5);
        $("#tablePeriodEnergy_7_5").html(tablePeriodEnergy_7_5);
        $("#tablePeriodEnergy_12_5").html(tablePeriodEnergy_12_5);
        $("#tablePeriodEnergy_17_5").html(tablePeriodEnergy_17_5);
        $("#tablePeriodEnergy_22_5").html(tablePeriodEnergy_22_5);
        $("#tablePeriodEnergy_27_5").html(tablePeriodEnergy_27_5);
        $("#tablePeriodEnergy_42_5").html(tablePeriodEnergy_42_5);
        $("#tablePeriodEnergy_47_5").html(tablePeriodEnergy_47_5);
        $("#tablePeriodEnergy_52_5").html(tablePeriodEnergy_52_5);
        $("#tablePeriodEnergy_57_5").html(tablePeriodEnergy_57_5);
        $("#tablePeriodEnergy_62_5").html(tablePeriodEnergy_62_5);
        $("#tablePeriodEnergy_67_5").html(tablePeriodEnergy_67_5);
        $("#tablePeriodEnergy_72_5").html(tablePeriodEnergy_72_5);
        $("#tablePeriodEnergy_77_5").html(tablePeriodEnergy_77_5);


        $("#tablePeriodEnergy_1_25").html(tablePeriodEnergy_1_25);
        $("#tablePeriodEnergy_3_75").html(tablePeriodEnergy_3_75);
        $("#tablePeriodEnergy_6_25").html(tablePeriodEnergy_6_25);
        $("#tablePeriodEnergy_8_75").html(tablePeriodEnergy_8_75);
        $("#tablePeriodEnergy_11_25").html(tablePeriodEnergy_11_25);
        $("#tablePeriodEnergy_13_75").html(tablePeriodEnergy_13_75);
        $("#tablePeriodEnergy_16_25").html(tablePeriodEnergy_16_25);
        $("#tablePeriodEnergy_18_75").html(tablePeriodEnergy_18_75);
        $("#tablePeriodEnergy_21_25").html(tablePeriodEnergy_21_25);
        $("#tablePeriodEnergy_23_75").html(tablePeriodEnergy_23_75);
        $("#tablePeriodEnergy_26_25").html(tablePeriodEnergy_26_25);
        $("#tablePeriodEnergy_28_75").html(tablePeriodEnergy_28_75);
        $("#tablePeriodEnergy_41_25").html(tablePeriodEnergy_41_25);
        $("#tablePeriodEnergy_43_75").html(tablePeriodEnergy_43_75);
        $("#tablePeriodEnergy_46_25").html(tablePeriodEnergy_46_25);
        $("#tablePeriodEnergy_48_75").html(tablePeriodEnergy_48_75);
        $("#tablePeriodEnergy_51_25").html(tablePeriodEnergy_51_25);
        $("#tablePeriodEnergy_53_75").html(tablePeriodEnergy_53_75);
        $("#tablePeriodEnergy_56_25").html(tablePeriodEnergy_56_25);
        $("#tablePeriodEnergy_58_75").html(tablePeriodEnergy_58_75);
        $("#tablePeriodEnergy_61_25").html(tablePeriodEnergy_61_25);
        $("#tablePeriodEnergy_63_75").html(tablePeriodEnergy_63_75);
        $("#tablePeriodEnergy_66_25").html(tablePeriodEnergy_66_25);
        $("#tablePeriodEnergy_68_75").html(tablePeriodEnergy_68_75);
        $("#tablePeriodEnergy_71_25").html(tablePeriodEnergy_71_25);
        $("#tablePeriodEnergy_73_75").html(tablePeriodEnergy_73_75);
        $("#tablePeriodEnergy_76_25").html(tablePeriodEnergy_76_25);
        $("#tablePeriodEnergy_78_75").html(tablePeriodEnergy_78_75);

    });


    $("#FormCalc2").submit(function (e) {
        e.preventDefault();

        let isInvalid = false;
        var valueInputDate_1 = $("#FormCalc2 input[name=date1]").val();
        if (valueInputDate_1.length == 0) {
            $("#FormCalc2 input[name=date1]").addClass("is-invalid");
            console.log("valueInputDate_1.length");
            isInvalid = true;
        } else {
            $("#FormCalc2 input[name=date1]").removeClass("is-invalid");
        }
        var valueInputDate_2 = $("#FormCalc2 input[name=date2]").val();
        if (valueInputDate_2.length == 0) {
            $("#FormCalc2 input[name=date2]").addClass("is-invalid");
            console.log(valueInputDate_2.length == 0);
            isInvalid = true;
        } else {
            $("#FormCalc2 input[name=date2]").removeClass("is-invalid");
        }
        if (isInvalid) {
            alert("Не верный формат даты! Пример: 01.01.2020");
            return;
        }




        var dateOfBirth_1 = $(this).find("input[name=date1]").val();
        var dateOfBirth_2 = $(this).find("input[name=date2]").val();


        // DEV1
        var pointsMatrix1 = calcMatrixForString(dateOfBirth_1);
        var pointsMatrix2 = calcMatrixForString(dateOfBirth_2);

        var pointsMatrix = calcMatrixCompatibility(pointsMatrix1, pointsMatrix2, dateOfBirth_1, dateOfBirth_2);

        var point2_01 = pointsMatrix.get("point_01");
        console.log(point2_01);
        $("#point2_01").html(point2_01);

        var point2_02 = pointsMatrix.get("point_02");
        $("#point2_02").html(point2_02);


        var point2_03 = pointsMatrix.get("point_03");
        $("#point2_03").html(point2_03);

        var point2_04 = pointsMatrix.get("point_04");
        $("#point2_04").html(point2_04);

        var point2_05 = pointsMatrix.get("point_05");
        $("#point2_05").html(point2_05);

        var point2_08 = pointsMatrix.get("point_08");
        $("#point2_08").html(point2_08);

        var point2_09 = pointsMatrix.get("point_09");
        $("#point2_09").html(point2_09);

        var point2_10 = pointsMatrix.get("point_10");
        $("#point2_10").html(point2_10);

        var point2_11 = pointsMatrix.get("point_11");
        $("#point2_11").html(point2_11);

        var point2_12 = pointsMatrix.get("point_12");
        $("#point2_12").html(point2_12);

        var point2_13 = pointsMatrix.get("point_13");
        $("#point2_13").html(point2_13);

        var point2_18 = pointsMatrix.get("point_18");
        $("#point2_18").html(point2_18);

        var point2_19 = pointsMatrix.get("point_19");
        $("#point2_19").html(point2_19);

        var point2_20 = pointsMatrix.get("point_20");
        $("#point2_20").html(point2_20);

        var point2_21 = pointsMatrix.get("point_21");
        $("#point2_21").html(point2_21);



        // Предназначение для себя - Небо
        // #SpanDestinyMySky2
        // Складываем вторую и четвертую точку
        var destinyMySky2 = fixedNumberForRange(parseInt(point2_02) + parseInt(point2_04));
        $("#SpanDestinyMySky2").html(destinyMySky2);

        // Предназначение для себя - Земля
        // #SpanDestinyMyEarth2
        // Складываем первую и третью точку
        var destinyMyEarth2 = fixedNumberForRange(parseInt(point2_01) + parseInt(point2_03));
        $("#SpanDestinyMyEarth2").html(destinyMyEarth2);

        // Предназначение для себя
        // #SpanDestinyMySum
        // Складываем предназначение для себя по небу и предназначение для себя по земле
        var destinyMySum2 = fixedNumberForRange(parseInt(destinyMySky2) + parseInt(destinyMyEarth2), 22);
        $("#SpanDestinyMySum2").html(destinyMySum2);

        // Предназначение для социума - М
        // #SpanDestinySocietyM2
        var destinySocietyM2 = fixedNumberForRange(parseInt(point2_20) + parseInt(point2_18));
        $("#SpanDestinySocietyM2").html(destinySocietyM2);

        // Предназначение для социума - Ж
        // #SpanDestinySocietyZh2
        var destinySocietyZh2 = fixedNumberForRange(parseInt(point2_21) + parseInt(point2_19));
        $("#SpanDestinySocietyZh2").html(destinySocietyZh2);

        // Предназначение для социума
        // складываем предназначение для социума М и предназначение для социума Ж
        var destinySocietySum2 = fixedNumberForRange(parseInt(destinySocietyM2) + parseInt(destinySocietyZh2), 22);
        $("#SpanDestinySocietySum2").html(destinySocietySum2);

        // Общее предназначение (в примере цифра 12)
        // Складываем предназначение для себя третье и предназначение для социума третье

        var destinyGeneral2 = fixedNumberForRange(parseInt(destinyMySum2) + parseInt(destinySocietySum2), 22);
        $("#SpanDestinyGeneral2").html(destinyGeneral2);

    });
});



function fixedNumberForRange(num, range = 22) {
    num = parseInt(num);
    range = parseInt(range);

    if (num > range) {
        var str = num.toString();
        return parseInt(str[0]) + parseInt(str[1]);
    } else {
        return num;
    }
}

function calcValue(valuePhysics, valueEnergy) {
    let physics = parseInt(valuePhysics.toString());
    let energy = parseInt(valueEnergy.toString());
    let healthKey = physics + energy;
    if (healthKey > 22) {
        let str = healthKey.toString();
        healthKey = parseInt(str[0]) + parseInt(str[1]);
    }
    return healthKey;
}


function calcTotalCardHealth(row1, row2, row3, row4, row5, row6, row7) {
    let total = 0;
    total += parseInt(row1.toString());
    total += parseInt(row2.toString());
    total += parseInt(row3.toString());
    total += parseInt(row4.toString());
    total += parseInt(row5.toString());
    total += parseInt(row6.toString());
    total += parseInt(row7.toString());
    if (total > 22) {
        let str = total.toString();
        if (str.length == 2) {
            total = parseInt(str[0]) + parseInt(str[1]);
        }
        if (str.length == 3) {
            total = parseInt(str[0]) + parseInt(str[1]) + parseInt(str[2]);
        }
    }
    return total;
}

/**
 * Расчет матрицы души
 * @param {*} strDate 
 * @returns 
 */
function calcMatrixForString(strDate) {
    var aDate = strDate.split('.');
    if (aDate.length != 3) {
        console.log("aDate.length != 3");
        isInvalid = true;
    }
    var nDay = parseInt(aDate[0]);
    var nMonth = parseInt(aDate[1]);
    var nYear = parseInt(aDate[2]);
    return calcMatrix(nDay, nMonth, nYear);
}

/**
 * Расчет матрицы души
 * @param {ц} nDay 
 * @param {*} nMonth 
 * @param {*} nYear 
 * @returns 
 */
function calcMatrix(nDay, nMonth, nYear) {

    let points = new Map();

    // Первая точка (фиолетовый круг, в примере цифра 21)
    // Это дата рождения с 1 до 22, если больше 22 – то есть: 23 – это 2+3 = 5 – вписывается цифра 5 и т.д.
    var point_01 = fixedNumberForRange(nDay, 22);
    points.set("point_01", point_01);

    // Вторая точка (фиолетовый круг, в примере цифра 10)
    // Это месяц рождения с 1 до 12
    var point_02 = nMonth;
    points.set("point_02", point_02);

    // Третья точка (красный круг, в примере цифра 7)
    // Это год рождения, вычисляется путем сложения: 
    // В нашем примере 1987 – то есть 1+ 9 + 8 + 7 = 25 и полученную цифру складываем пока не получится цифра в диапазоне от 1 до 22 включительно! 
    var point_03 = 0;
    var sYear = nYear.toString();
    for (let i = 0; i < sYear.length; i++) {
        point_03 += parseInt(sYear[i]);
    }
    point_03 = fixedNumberForRange(point_03, 22);
    points.set("point_03", point_03);

    // Четвертая точка (красный круг, в примере цифра 11)
    // Это сложение первых трех точек
    // В нашем примере это: 21+10+7 = 38, получилась цифра большее 22 - поэтому далее сводим путем сложения пока не получится цифра в диапазоне от 1 до 22: 3+8 = 11
    var point_04 = parseInt(point_01) + parseInt(point_02) + parseInt(point_03);
    point_04 = fixedNumberForRange(point_04, 22);
    points.set("point_04", point_04);

    // Пятая точка (желтый круг по центру, в примере цифра 13)
    // #point_05
    // Это сложение первых четырех точек
    // В нашем примере это: 21+10+7+11 = 49, получилась цифра большее 22 - поэтому далее сводим путем сложения пока не получится цифра в диапазоне от 1 до 22: 4+9 = 13
    var point_05 = parseInt(point_01) + parseInt(point_02) + parseInt(point_03) + parseInt(point_04);;
    point_05 = fixedNumberForRange(point_05, 22);
    points.set("point_05", point_05);

    // Шестая точка (голубой круг, в примере цифра 5)
    // #point_06
    // Это сложение 2ой и 5ой точки
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_06 = parseInt(point_02) + parseInt(point_05);
    point_06 = fixedNumberForRange(point_06, 22);
    points.set("point_06", point_06);

    // Седьмая точка (голубой круг, в примере цифра 7)
    // #point_07
    // Это сложение 1ой и 5ой точки
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_07 = parseInt(point_01) + parseInt(point_05);
    point_07 = fixedNumberForRange(point_07, 22);
    points.set("point_07", point_07);

    // Восьмая точка (оранжевый круг, в примере цифра 20)
    // #point_08
    // Складываем пятую (#point_05) точку и третью точку (#point_03)
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_08 = parseInt(point_05) + parseInt(point_03);
    point_08 = fixedNumberForRange(point_08, 22);
    points.set("point_08", point_08);

    // Девятая точка – (оранжевый круг, в примере цифра 6)
    // #point_09
    // Складываем пятую (#point_05) точку и четвертую точку (#point_04)
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_09 = parseInt(point_05) + parseInt(point_04);
    point_09 = fixedNumberForRange(point_09, 22);
    points.set("point_09", point_09);

    // Десятая точка (бесцветный круг, в примере цифра 8)
    // #point_10
    // Складываем точку #point_08 и точку #point_09
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_10 = parseInt(point_08) + parseInt(point_09);
    point_10 = fixedNumberForRange(point_10, 22);
    points.set("point_10", point_10);

    // Одиннадцатая точка (бесцветный круг, в примере цифра 10)
    // #point_11
    // Складываем точку #point_08 и точку #point_10
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_11 = parseInt(point_08) + parseInt(point_10);
    point_11 = fixedNumberForRange(point_11, 22);
    points.set("point_11", point_11);

    // Двенадцатая точка – (оранжевый круг, в примере цифра 14)
    // #point_12
    // Складываем точку #point_09 и точку #point_10
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_12 = parseInt(point_09) + parseInt(point_10);
    point_12 = fixedNumberForRange(point_12, 22);
    points.set("point_12", point_12);

    // Тринадцатая точка (бесцветный круг, в примере цифра 17) 
    // #point_13
    // Складываем четвертую точку и девятую точку
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_13 = parseInt(point_04) + parseInt(point_09);
    point_13 = fixedNumberForRange(point_13, 22);
    points.set("point_13", point_13);

    // Четырнадцатая точка (синий круг, в примере цифра 15)
    // #point_14
    // Складываем вторую точку и шестую
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_14 = parseInt(point_02) + parseInt(point_06);
    point_14 = fixedNumberForRange(point_14, 22);
    points.set("point_14", point_14);

    // Пятнадцатая точка – (синий круг, в примере цифра 10)
    // #point_15
    // Складываем первую и седьмую точку
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_15 = parseInt(point_01) + parseInt(point_07);
    point_15 = fixedNumberForRange(point_15, 22);
    points.set("point_15", point_15);

    // Шестнадцатая точка (зеленый круг, в примере цифра 18)
    // #point_16
    // Складываем пятую центральную точку и шестую
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_16 = parseInt(point_05) + parseInt(point_06);
    point_16 = fixedNumberForRange(point_16, 22);
    points.set("point_16", point_16);

    // Семнадцатая точка (зеленый круг, в примере цифра 20)
    // #point_17
    // Складываем пятую центральную точку и седьмую
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_17 = parseInt(point_05) + parseInt(point_07);
    point_17 = fixedNumberForRange(point_17, 22);
    points.set("point_17", point_17);

    // Восемнадцатая точка – (бесцветный круг, в примере цифра 4)
    // #point_18
    // Складываем первую и вторую точку
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_18 = parseInt(point_01) + parseInt(point_02);
    point_18 = fixedNumberForRange(point_18, 22);
    points.set("point_18", point_18);

    // Девятнадцатая точка (бесцветный круг, в примере цифра 17)
    // #point_19
    // Складываем вторую и третью точку
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_19 = parseInt(point_02) + parseInt(point_03);
    point_19 = fixedNumberForRange(point_19, 22);
    points.set("point_19", point_19);

    // Двадцатая точка (бесцветный круг, в примере цифра 18)
    // #point_20
    // Складываем третью и четвертую точку
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_20 = parseInt(point_03) + parseInt(point_04);
    point_20 = fixedNumberForRange(point_20, 22);
    points.set("point_20", point_20);

    // Двадцать первая точка – (бесцветный круг, в примере цифра 5)
    // #point_21
    // Складываем первую и четвертую точку
    // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
    var point_21 = parseInt(point_01) + parseInt(point_04);
    point_21 = fixedNumberForRange(point_21, 22);
    points.set("point_21", point_21);

    return points;
}

/**
 * Расчет значений для предназначения
 * @param {*} pointsMatrix Значение точек матрицы души
 * @returns 
 */
function calcPurpose(pointsMatrix) {
    var points = new Map();
    // Предназначение для себя - Небо
    // Складываем 2 и 4 точки
    var destinyMySky = parseInt(pointsMatrix.get("point_02")) + parseInt(pointsMatrix.get("point_04"));
    destinyMySky = fixedNumberForRange(destinyMySky);
    points.set("SpanDestinyMySky", destinyMySky);
    // Предназначение для себя - Земля
    // Складываем 1 и 3 точки
    var destinyMyEarth = parseInt(pointsMatrix.get("point_01")) + parseInt(pointsMatrix.get("point_03"));
    destinyMyEarth = fixedNumberForRange(destinyMyEarth);
    points.set("SpanDestinyMyEarth", destinyMyEarth);
    // Предназначение для себя
    // Складываем предназначение для себя по небу и предназначение для себя по земле
    var destinyMySum = parseInt(destinyMySky) + parseInt(destinyMyEarth);
    destinyMySum = fixedNumberForRange(destinyMySum, 22);
    points.set("SpanDestinyMySum", destinyMySum);
    // Предназначение для социума - М
    // Складываем 20 и 18 точки
    var destinySocietyM = parseInt(pointsMatrix.get("point_20")) + parseInt(pointsMatrix.get("point_18"));
    destinySocietyM = fixedNumberForRange(destinySocietyM);
    points.set("SpanDestinySocietyM", destinySocietyM);
    // Предназначение для социума - Ж
    // Складываем 21 и 19 точки
    var destinySocietyZh = parseInt(pointsMatrix.get("point_21")) + parseInt(pointsMatrix.get("point_19"));
    destinySocietyZh = fixedNumberForRange(destinySocietyZh);
    points.set("SpanDestinySocietyZh", destinySocietyZh);
    // Предназначение для социума
    // складываем предназначение для социума М и предназначение для социума Ж
    var destinySocietySum = parseInt(destinySocietyM) + parseInt(destinySocietyZh);
    destinySocietySum = fixedNumberForRange(destinySocietySum, 22);
    points.set("SpanDestinySocietySum", SpanDestinySocietySum);
    // Общее предназначение (в примере цифра 12)
    // Складываем предназначение для себя третье и предназначение для социума третье
    var destinyGeneral = parseInt(destinyMySum) + parseInt(destinySocietySum);
    destinyGeneral = fixedNumberForRange(destinyGeneral, 22);
    points.set("SpanDestinyGeneral",);
    return points;
}

/**
* Расчет матрицы совместимости
*/
function calcMatrixCompatibility(pointsMatrix1, pointsMatrix2, dateOfBirth_1, dateOfBirth_2) {

    var aDate1 = dateOfBirth_1.split('.');
    if (aDate1.length != 3) {
        console.log("aDate.length != 3");
        isInvalid = true;
    }
    var nDay1 = parseInt(aDate1[0]);
    var nMonth1 = parseInt(aDate1[1]);
    var nYear1 = parseInt(aDate1[2]);



    var aDate2 = dateOfBirth_2.split('.');
    if (aDate2.length != 3) {
        console.log("aDate.length != 3");
        isInvalid = true;
    }
    var nDay2 = parseInt(aDate2[0]);
    var nMonth2 = parseInt(aDate2[1]);
    var nYear2 = parseInt(aDate2[2]);


    var points = new Map();

    // вычисляемые точки на основе двух матриц
    // DEV2
    var point_01 = calcValue(nDay1, nDay2);

    //console.log([pointsMatrix1.get("point_01"), pointsMatrix2.get("point_01"), point_01]);

    points.set("point_01", point_01);

    var point_02 = calcValue(pointsMatrix1.get("point_02"), pointsMatrix2.get("point_02"));
    points.set("point_02", point_02);

    point_03 = calcValue(pointsMatrix1.get("point_03"), pointsMatrix2.get("point_03"));
    points.set("point_03", point_03);

    var point_04 = calcValue(pointsMatrix1.get("point_04"), pointsMatrix2.get("point_04"));
    points.set("point_04", point_04);

    var point_18 = calcValue(pointsMatrix1.get("point_18"), pointsMatrix2.get("point_18"));
    points.set("point_18", point_18);

    var point_19 = calcValue(pointsMatrix1.get("point_19"), pointsMatrix2.get("point_19"));
    points.set("point_19", point_19);

    var point_20 = calcValue(pointsMatrix1.get("point_20"), pointsMatrix2.get("point_20"));
    points.set("point_20", point_20);


    // Вычисляемые точки на основе ранее вычисленных точки матрицы
    var point_21 = calcValue(pointsMatrix1.get("point_21"), pointsMatrix2.get("point_21"));
    points.set("point_21", point_21);

    var point_05 = parseInt(point_01) + parseInt(point_02) + parseInt(point_03) + parseInt(point_04);
    point_05 = fixedNumberForRange(point_05, 22);
    points.set("point_05", point_05);

    var point_06 = parseInt(point_02) + parseInt(point_05);
    point_06 = fixedNumberForRange(point_06, 22);
    points.set("point_06", point_06);

    var point_07 = parseInt(point_01) + parseInt(point_05);
    point_07 = fixedNumberForRange(point_07, 22);
    points.set("point_07", point_07);

    var point_08 = parseInt(point_05) + parseInt(point_03);
    point_08 = fixedNumberForRange(point_08, 22);
    points.set("point_08", point_08);

    var point_09 = parseInt(point_05) + parseInt(point_04);
    point_09 = fixedNumberForRange(point_09, 22);
    points.set("point_09", point_09);

    var point_10 = parseInt(point_08) + parseInt(point_09);
    point_10 = fixedNumberForRange(point_10, 22);
    points.set("point_10", point_10);

    var point_11 = parseInt(point_08) + parseInt(point_10);
    point_11 = fixedNumberForRange(point_11, 22);
    points.set("point_11", point_11);

    var point_12 = parseInt(point_09) + parseInt(point_10);
    point_12 = fixedNumberForRange(point_12, 22);
    points.set("point_12", point_12);

    var point_13 = parseInt(point_04) + parseInt(point_09);
    point_13 = fixedNumberForRange(point_13, 22);
    points.set("point_13", point_13);

    var point_14 = parseInt(point_02) + parseInt(point_06);
    point_14 = fixedNumberForRange(point_14, 22);
    points.set("point_14", point_14);

    var point_15 = parseInt(point_01) + parseInt(point_07);
    point_15 = fixedNumberForRange(point_15, 22);
    points.set("point_15", point_15);

    var point_16 = parseInt(point_05) + parseInt(point_06);
    point_16 = fixedNumberForRange(point_16, 22);
    points.set("point_16", point_16);

    var point_17 = parseInt(point_05) + parseInt(point_07);
    point_17 = fixedNumberForRange(point_17, 22);
    points.set("point_17", point_17);

    return points;
}